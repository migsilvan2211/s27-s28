const http = require('http');
let port = 4000;

let usersCollection = require('./users.json');
let moviesCollection = require('./movies.json');
let coursesCollection = require('./courses.json');

http.createServer((req, res) => {

	
	if(req.url === '/' && req.method === 'GET') {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write("Welcome to Miguel's Server");
		res.end();
	}
	else if(req.url === '/movies' && req.method === 'GET') {
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(moviesCollection));
		res.end();
	}
	else if(req.url === '/courses' && req.method === 'GET') {
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(coursesCollection));
		res.end();
	}
	else if(req.url === '/add-course' && req.method === 'POST') {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write("Add a course to our resources");
		res.end();
	}
	else if(req.url === '/users' && req.method === 'GET') {
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(usersCollection));
		res.end();
	}
	else if(req.url === '/add-movie' && req.method === 'POST') {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write("Add a movie to our resources");
		res.end();
	}	
	else if(req.url === '/register' && req.method === 'POST') {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write("Added user to our resources");
		res.end();
	}
	else {
		res.writeHead(404, {'Content-type': 'text/plain'});
		res.write("Error 404: Page not Found");
		res.end();
	}

}).listen(port);